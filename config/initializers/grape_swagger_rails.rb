GrapeSwaggerRails.options.url      = '/api/swagger_doc.json'
GrapeSwaggerRails.options.app_name = 'Trip Accountant'
GrapeSwaggerRails.options.app_url  = '/'

GrapeSwaggerRails.options.api_key_name = 'api_key'
GrapeSwaggerRails.options.api_key_type = 'query'