TripAccountant::Application.routes.draw do
  resources :api_users


  root :to => 'welcome#index'

  mount API::Base => '/api'
  mount GrapeSwaggerRails::Engine => '/apidoc'
end
