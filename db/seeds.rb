# This file should contain all the record creation needed to seed the database with its default amounts.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# Create country information
countries = [
  { 'country_name' => 'AFGHANISTAN                                 ', 'iso_code' => 'AF', 'id' =>   8  },
  { 'country_name' => 'ALBANIA                                     ', 'iso_code' => 'AL', 'id' =>  12  },
  { 'country_name' => 'ALGERIA                                     ', 'iso_code' => 'DZ', 'id' =>  16  },
  { 'country_name' => 'AMERICAN SAMOA                              ', 'iso_code' => 'AS', 'id' =>  20  },
  { 'country_name' => 'ANDORRA                                     ', 'iso_code' => 'AD', 'id' =>  24  },
  { 'country_name' => 'ANGOLA                                      ', 'iso_code' => 'AO', 'id' =>  660 },
  { 'country_name' => 'ANGUILLA                                    ', 'iso_code' => 'AI', 'id' =>  10  },
  { 'country_name' => 'ANTARCTICA                                  ', 'iso_code' => 'AQ', 'id' =>  28  },
  { 'country_name' => 'ANTIGUA AND BARBUDA                         ', 'iso_code' => 'AG', 'id' =>  32  },
  { 'country_name' => 'ARGENTINA                                   ', 'iso_code' => 'AR', 'id' =>  51  },
  { 'country_name' => 'ARMENIA                                     ', 'iso_code' => 'AM', 'id' =>  533 },  
  { 'country_name' => 'ARUBA                                       ', 'iso_code' => 'AW', 'id' =>  36  },
  { 'country_name' => 'AUSTRALIA                                   ', 'iso_code' => 'AU', 'id' =>  40  },
  { 'country_name' => 'AUSTRIA                                     ', 'iso_code' => 'AT', 'id' =>  31  },
  { 'country_name' => 'AZERBAIJAN                                  ', 'iso_code' => 'AZ', 'id' =>  44  },  
  { 'country_name' => 'BAHAMAS                                     ', 'iso_code' => 'BS', 'id' =>  48  },
  { 'country_name' => 'BAHRAIN                                     ', 'iso_code' => 'BH', 'id' =>  50  },
  { 'country_name' => 'BANGLADESH                                  ', 'iso_code' => 'BD', 'id' =>  52  },
  { 'country_name' => 'BARBADOS                                    ', 'iso_code' => 'BB', 'id' =>  112 },
  { 'country_name' => 'BELARUS                                     ', 'iso_code' => 'BY', 'id' =>  56 },  
  { 'country_name' => 'BELGIUM                                     ', 'iso_code' => 'BE', 'id' =>  84 },
  { 'country_name' => 'BELIZE                                      ', 'iso_code' => 'BZ', 'id' =>  204 },
  { 'country_name' => 'BENIN                                       ', 'iso_code' => 'BJ', 'id' =>  60 },
  { 'country_name' => 'BERMUDA                                     ', 'iso_code' => 'BM', 'id' =>  64 },
  { 'country_name' => 'BHUTAN                                      ', 'iso_code' => 'BT', 'id' =>  68 },
  { 'country_name' => 'BOLIVIA                                     ', 'iso_code' => 'BO', 'id' =>  70 },
  { 'country_name' => 'BOSNIA AND HERZEGOWINA                      ', 'iso_code' => 'BA', 'id' =>  72 },
  { 'country_name' => 'BOTSWANA                                    ', 'iso_code' => 'BW', 'id' =>  74 },
  { 'country_name' => 'BOUVET ISLAND                               ', 'iso_code' => 'BV', 'id' =>  76 },
  { 'country_name' => 'BRAZIL                                      ', 'iso_code' => 'BR', 'id' =>  86 },
  { 'country_name' => 'BRITISH INDIAN OCEAN TERRITORY              ', 'iso_code' => 'IO', 'id' =>  96 },
  { 'country_name' => 'BRUNEI DARUSSALAM                           ', 'iso_code' => 'BN', 'id' =>  100 },
  { 'country_name' => 'BULGARIA                                    ', 'iso_code' => 'BG', 'id' =>  854 },
  { 'country_name' => 'BURKINA FASO                                ', 'iso_code' => 'BF', 'id' =>  108 },
  { 'country_name' => 'BURUNDI                                     ', 'iso_code' => 'BI', 'id' =>  116 },
  { 'country_name' => 'CAMBODIA                                    ', 'iso_code' => 'KH', 'id' =>  120 },
  { 'country_name' => 'CAMEROON                                    ', 'iso_code' => 'CM', 'id' =>  124 },
  { 'country_name' => 'CANADA                                      ', 'iso_code' => 'CA', 'id' =>  132 },
  { 'country_name' => 'CAPE VERDE                                  ', 'iso_code' => 'CV', 'id' =>  136 },
  { 'country_name' => 'CAYMAN ISLANDS                              ', 'iso_code' => 'KY', 'id' =>  140 },
  { 'country_name' => 'CENTRAL AFRICAN REPUBLIC                    ', 'iso_code' => 'CF', 'id' =>  148 },
  { 'country_name' => 'CHAD                                        ', 'iso_code' => 'TD', 'id' =>  152 },
  { 'country_name' => 'CHILE                                       ', 'iso_code' => 'CL', 'id' =>  156 },
  { 'country_name' => 'CHINA                                       ', 'iso_code' => 'CN', 'id' =>  162 },
  { 'country_name' => 'CHRISTMAS ISLAND                            ', 'iso_code' => 'CX', 'id' =>  166 },
  { 'country_name' => 'COCOS (KEELING) ISLANDS                     ', 'iso_code' => 'CC', 'id' =>  170 },
  { 'country_name' => 'COLOMBIA                                    ', 'iso_code' => 'CO', 'id' =>  174 },
  { 'country_name' => 'COMOROS                                     ', 'iso_code' => 'KM', 'id' =>  180 },
  { 'country_name' => 'CONGO, Democratic Republic of (was Zaire)   ', 'iso_code' => 'CD', 'id' =>  178 },
  { 'country_name' => 'CONGO, Republic of                          ', 'iso_code' => 'CG', 'id' =>  184 },
  { 'country_name' => 'COOK ISLANDS                                ', 'iso_code' => 'CK', 'id' =>  188 },
  { 'country_name' => 'COSTA RICA                                  ', 'iso_code' => 'CR', 'id' =>  384 },
  { 'country_name' => 'CROATIA (local name: Hrvatska)              ', 'iso_code' => 'HR', 'id' =>  191 },      
  { 'country_name' => 'CUBA                                        ', 'iso_code' => 'CU', 'id' =>  192 },
  { 'country_name' => 'CYPRUS                                      ', 'iso_code' => 'CY', 'id' =>  196 },
  { 'country_name' => 'CZECH REPUBLIC                              ', 'iso_code' => 'CZ', 'id' =>  203 },  
  { 'country_name' => 'DENMARK                                     ', 'iso_code' => 'DK', 'id' =>  208 },
  { 'country_name' => 'DJIBOUTI                                    ', 'iso_code' => 'DJ', 'id' =>  262 },
  { 'country_name' => 'DOMINICA                                    ', 'iso_code' => 'DM', 'id' =>  212 },
  { 'country_name' => 'DOMINICAN REPUBLIC                          ', 'iso_code' => 'DO', 'id' =>  214 },
  { 'country_name' => 'ECUADOR                                     ', 'iso_code' => 'EC', 'id' =>  218 },
  { 'country_name' => 'EGYPT                                       ', 'iso_code' => 'EG', 'id' =>  818 },
  { 'country_name' => 'EL SALVADOR                                 ', 'iso_code' => 'SV', 'id' =>  222 },
  { 'country_name' => 'EQUATORIAL GUINEA                           ', 'iso_code' => 'GQ', 'id' =>  226 },
  { 'country_name' => 'ERITREA                                     ', 'iso_code' => 'ER', 'id' =>  232 },
  { 'country_name' => 'ESTONIA                                     ', 'iso_code' => 'EE', 'id' =>  233 },  
  { 'country_name' => 'ETHIOPIA                                    ', 'iso_code' => 'ET', 'id' =>  231 },
  { 'country_name' => 'FALKLAND ISLANDS (MALVINAS)                 ', 'iso_code' => 'FK', 'id' =>  238 },
  { 'country_name' => 'FAROE ISLANDS                               ', 'iso_code' => 'FO', 'id' =>  234 },
  { 'country_name' => 'FIJI                                        ', 'iso_code' => 'FJ', 'id' =>  242 },
  { 'country_name' => 'FINLAND                                     ', 'iso_code' => 'FI', 'id' =>  246 },
  { 'country_name' => 'FRANCE                                      ', 'iso_code' => 'FR', 'id' =>  250 },
  { 'country_name' => 'FRENCH GUIANA                               ', 'iso_code' => 'GF', 'id' =>  254 },
  { 'country_name' => 'FRENCH POLYNESIA                            ', 'iso_code' => 'PF', 'id' =>  258 },
  { 'country_name' => 'FRENCH SOUTHERN TERRITORIES                 ', 'iso_code' => 'TF', 'id' =>  260 },
  { 'country_name' => 'GABON                                       ', 'iso_code' => 'GA', 'id' =>  266 },
  { 'country_name' => 'GAMBIA                                      ', 'iso_code' => 'GM', 'id' =>  270 },
  { 'country_name' => 'GEORGIA                                     ', 'iso_code' => 'GE', 'id' =>  268 },  
  { 'country_name' => 'GERMANY                                     ', 'iso_code' => 'DE', 'id' =>  276 },
  { 'country_name' => 'GHANA                                       ', 'iso_code' => 'GH', 'id' =>  288 },
  { 'country_name' => 'GIBRALTAR                                   ', 'iso_code' => 'GI', 'id' =>  292 },
  { 'country_name' => 'GREECE                                      ', 'iso_code' => 'GR', 'id' =>  300 },
  { 'country_name' => 'GREENLAND                                   ', 'iso_code' => 'GL', 'id' =>  304 },
  { 'country_name' => 'GRENADA                                     ', 'iso_code' => 'GD', 'id' =>  308 },
  { 'country_name' => 'GUADELOUPE                                  ', 'iso_code' => 'GP', 'id' =>  312 },
  { 'country_name' => 'GUAM                                        ', 'iso_code' => 'GU', 'id' =>  316 },
  { 'country_name' => 'GUATEMALA                                   ', 'iso_code' => 'GT', 'id' =>  320 },
  { 'country_name' => 'GUINEA                                      ', 'iso_code' => 'GN', 'id' =>  324 },
  { 'country_name' => 'GUINEA-BISSAU                               ', 'iso_code' => 'GW', 'id' =>  624 },
  { 'country_name' => 'GUYANA                                      ', 'iso_code' => 'GY', 'id' =>  328 },
  { 'country_name' => 'HAITI                                       ', 'iso_code' => 'HT', 'id' =>  332 },
  { 'country_name' => 'HEARD AND MC DONALD ISLANDS                 ', 'iso_code' => 'HM', 'id' =>  334 },
  { 'country_name' => 'HONDURAS                                    ', 'iso_code' => 'HN', 'id' =>  340 },
  { 'country_name' => 'HONG KONG                                   ', 'iso_code' => 'HK', 'id' =>  344 },
  { 'country_name' => 'HUNGARY                                     ', 'iso_code' => 'HU', 'id' =>  348 },
  { 'country_name' => 'ICELAND                                     ', 'iso_code' => 'IS', 'id' =>  352 },
  { 'country_name' => 'INDIA                                       ', 'iso_code' => 'IN', 'id' =>  356 },
  { 'country_name' => 'INDONESIA                                   ', 'iso_code' => 'ID', 'id' =>  360 },
  { 'country_name' => 'IRAN (ISLAMIC REPUBLIC OF)                  ', 'iso_code' => 'IR', 'id' =>  364 },
  { 'country_name' => 'IRAQ                                        ', 'iso_code' => 'IQ', 'id' =>  368 },
  { 'country_name' => 'IRELAND                                     ', 'iso_code' => 'IE', 'id' =>  372 },
  { 'country_name' => 'ISRAEL                                      ', 'iso_code' => 'IL', 'id' =>  376 },
  { 'country_name' => 'ITALY                                       ', 'iso_code' => 'IT', 'id' =>  380 },
  { 'country_name' => 'JAMAICA                                     ', 'iso_code' => 'JM', 'id' =>  388 },
  { 'country_name' => 'JAPAN                                       ', 'iso_code' => 'JP', 'id' =>  392 },
  { 'country_name' => 'JORDAN                                      ', 'iso_code' => 'JO', 'id' =>  400 },
  { 'country_name' => 'KAZAKHSTAN                                  ', 'iso_code' => 'KZ', 'id' =>  398 },  
  { 'country_name' => 'KENYA                                       ', 'iso_code' => 'KE', 'id' =>  404 },
  { 'country_name' => 'KIRIBATI                                    ', 'iso_code' => 'KI', 'id' =>  296 },
  { 'country_name' => 'KOREA, DEMOCRATIC PEOPLES REPUBLIC OF       ', 'iso_code' => 'KP', 'id' =>  408 },
  { 'country_name' => 'KOREA, REPUBLIC OF                          ', 'iso_code' => 'KR', 'id' =>  410 },
  { 'country_name' => 'KUWAIT                                      ', 'iso_code' => 'KW', 'id' =>  414 },
  { 'country_name' => 'KYRGYZSTAN                                  ', 'iso_code' => 'KG', 'id' =>  417 },  
  { 'country_name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC             ', 'iso_code' => 'LA', 'id' =>  418 },
  { 'country_name' => 'LATVIA                                      ', 'iso_code' => 'LV', 'id' =>  428 },  
  { 'country_name' => 'LEBANON                                     ', 'iso_code' => 'LB', 'id' =>  422 },
  { 'country_name' => 'LESOTHO                                     ', 'iso_code' => 'LS', 'id' =>  426 },
  { 'country_name' => 'LIBERIA                                     ', 'iso_code' => 'LR', 'id' =>  430 },
  { 'country_name' => 'LIBYAN ARAB JAMAHIRIYA                      ', 'iso_code' => 'LY', 'id' =>  434 },
  { 'country_name' => 'LIECHTENSTEIN                               ', 'iso_code' => 'LI', 'id' =>  438 },
  { 'country_name' => 'LITHUANIA                                   ', 'iso_code' => 'LT', 'id' =>  440 },  
  { 'country_name' => 'LUXEMBOURG                                  ', 'iso_code' => 'LU', 'id' =>  442 },
  { 'country_name' => 'MACAU                                       ', 'iso_code' => 'MO', 'id' =>  446 },
  { 'country_name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF  ', 'iso_code' => 'MK', 'id' =>  807 }, 
  { 'country_name' => 'MADAGASCAR                                  ', 'iso_code' => 'MG', 'id' =>  450 },
  { 'country_name' => 'MALAWI                                      ', 'iso_code' => 'MW', 'id' =>  454 },
  { 'country_name' => 'MALAYSIA                                    ', 'iso_code' => 'MY', 'id' =>  458 },
  { 'country_name' => 'MALDIVES                                    ', 'iso_code' => 'MV', 'id' =>  462 },
  { 'country_name' => 'MALI                                        ', 'iso_code' => 'ML', 'id' =>  466 },
  { 'country_name' => 'MALTA                                       ', 'iso_code' => 'MT', 'id' =>  470 },
  { 'country_name' => 'MARSHALL ISLANDS                            ', 'iso_code' => 'MH', 'id' =>  584 },
  { 'country_name' => 'MARTINIQUE                                  ', 'iso_code' => 'MQ', 'id' =>  474 },
  { 'country_name' => 'MAURITANIA                                  ', 'iso_code' => 'MR', 'id' =>  478 },
  { 'country_name' => 'MAURITIUS                                   ', 'iso_code' => 'MU', 'id' =>  480 },
  { 'country_name' => 'MAYOTTE                                     ', 'iso_code' => 'YT', 'id' =>  175 },  
  { 'country_name' => 'MEXICO                                      ', 'iso_code' => 'MX', 'id' =>  484 },
  { 'country_name' => 'MICRONESIA, FEDERATED STATES OF             ', 'iso_code' => 'FM', 'id' =>  583 },
  { 'country_name' => 'MOLDOVA, REPUBLIC OF                        ', 'iso_code' => 'MD', 'id' =>  498 },  
  { 'country_name' => 'MONACO                                      ', 'iso_code' => 'MC', 'id' =>  492 },
  { 'country_name' => 'MONGOLIA                                    ', 'iso_code' => 'MN', 'id' =>  496 },
  { 'country_name' => 'MONTSERRAT                                  ', 'iso_code' => 'MS', 'id' =>  500 },
  { 'country_name' => 'MOROCCO                                     ', 'iso_code' => 'MA', 'id' =>  504 },
  { 'country_name' => 'MOZAMBIQUE                                  ', 'iso_code' => 'MZ', 'id' =>  508 },
  { 'country_name' => 'MYANMAR                                     ', 'iso_code' => 'MM', 'id' =>  104 },
  { 'country_name' => 'NAMIBIA                                     ', 'iso_code' => 'NA', 'id' =>  516 },
  { 'country_name' => 'NAURU                                       ', 'iso_code' => 'NR', 'id' =>  520 },
  { 'country_name' => 'NEPAL                                       ', 'iso_code' => 'NP', 'id' =>  524 },
  { 'country_name' => 'NETHERLANDS                                 ', 'iso_code' => 'NL', 'id' =>  528 },
  { 'country_name' => 'NETHERLANDS ANTILLES                        ', 'iso_code' => 'AN', 'id' =>  530 },
  { 'country_name' => 'NEW CALEDONIA                               ', 'iso_code' => 'NC', 'id' =>  540 },
  { 'country_name' => 'NEW ZEALAND                                 ', 'iso_code' => 'NZ', 'id' =>  554 },
  { 'country_name' => 'NICARAGUA                                   ', 'iso_code' => 'NI', 'id' =>  558 },
  { 'country_name' => 'NIGER                                       ', 'iso_code' => 'NE', 'id' =>  562 },
  { 'country_name' => 'NIGERIA                                     ', 'iso_code' => 'NG', 'id' =>  566 },
  { 'country_name' => 'NIUE                                        ', 'iso_code' => 'NU', 'id' =>  570 },
  { 'country_name' => 'NORFOLK ISLAND                              ', 'iso_code' => 'NF', 'id' =>  574 },
  { 'country_name' => 'NORTHERN MARIANA ISLANDS                    ', 'iso_code' => 'MP', 'id' =>  580 },
  { 'country_name' => 'NORWAY                                      ', 'iso_code' => 'NO', 'id' =>  578 },
  { 'country_name' => 'OMAN                                        ', 'iso_code' => 'OM', 'id' =>  512 },
  { 'country_name' => 'PAKISTAN                                    ', 'iso_code' => 'PK', 'id' =>  586 },
  { 'country_name' => 'PALAU                                       ', 'iso_code' => 'PW', 'id' =>  585 },
  { 'country_name' => 'PALESTINIAN TERRITORY, Occupied             ', 'iso_code' => 'PS', 'id' =>  275 },
  { 'country_name' => 'PANAMA                                      ', 'iso_code' => 'PA', 'id' =>  591 },
  { 'country_name' => 'PAPUA NEW GUINEA                            ', 'iso_code' => 'PG', 'id' =>  598 },
  { 'country_name' => 'PARAGUAY                                    ', 'iso_code' => 'PY', 'id' =>  600 },
  { 'country_name' => 'PERU                                        ', 'iso_code' => 'PE', 'id' =>  604 },
  { 'country_name' => 'PHILIPPINES                                 ', 'iso_code' => 'PH', 'id' =>  608 },
  { 'country_name' => 'PITCAIRN                                    ', 'iso_code' => 'PN', 'id' =>  612 },
  { 'country_name' => 'POLAND                                      ', 'iso_code' => 'PL', 'id' =>  616 },
  { 'country_name' => 'PORTUGAL                                    ', 'iso_code' => 'PT', 'id' =>  620 },
  { 'country_name' => 'PUERTO RICO                                 ', 'iso_code' => 'PR', 'id' =>  630 },
  { 'country_name' => 'QATAR                                       ', 'iso_code' => 'QA', 'id' =>  634 },
  { 'country_name' => 'REUNION                                     ', 'iso_code' => 'RE', 'id' =>  638 },
  { 'country_name' => 'ROMANIA                                     ', 'iso_code' => 'RO', 'id' =>  642 },
  { 'country_name' => 'RUSSIAN FEDERATION                          ', 'iso_code' => 'RU', 'id' =>  643 },
  { 'country_name' => 'RWANDA                                      ', 'iso_code' => 'RW', 'id' =>  646 },
  { 'country_name' => 'SAINT HELENA                                ', 'iso_code' => 'SH', 'id' =>  654 },
  { 'country_name' => 'SAINT KITTS AND NEVIS                       ', 'iso_code' => 'KN', 'id' =>  659 },
  { 'country_name' => 'SAINT LUCIA                                 ', 'iso_code' => 'LC', 'id' =>  662 },
  { 'country_name' => 'SAINT PIERRE AND MIQUELON                   ', 'iso_code' => 'PM', 'id' =>  666 },
  { 'country_name' => 'SAINT VINCENT AND THE GRENADINES            ', 'iso_code' => 'VC', 'id' =>  670 },
  { 'country_name' => 'SAMOA                                       ', 'iso_code' => 'WS', 'id' =>  882 },
  { 'country_name' => 'SAN MARINO                                  ', 'iso_code' => 'SM', 'id' =>  674 },
  { 'country_name' => 'SAO TOME AND PRINCIPE                       ', 'iso_code' => 'ST', 'id' =>  678 },
  { 'country_name' => 'SAUDI ARABIA                                ', 'iso_code' => 'SA', 'id' =>  682 },
  { 'country_name' => 'SENEGAL                                     ', 'iso_code' => 'SN', 'id' =>  686 },
  { 'country_name' => 'SERBIA AND MONTENEGRO                       ', 'iso_code' => 'CS', 'id' =>  891 },
  { 'country_name' => 'SEYCHELLES                                  ', 'iso_code' => 'SC', 'id' =>  690 },
  { 'country_name' => 'SIERRA LEONE                                ', 'iso_code' => 'SL', 'id' =>  694 },
  { 'country_name' => 'SINGAPORE                                   ', 'iso_code' => 'SG', 'id' =>  702 },
  { 'country_name' => 'SLOVAKIA                                    ', 'iso_code' => 'SK', 'id' =>  703 },  
  { 'country_name' => 'SLOVENIA                                    ', 'iso_code' => 'SI', 'id' =>  705 },  
  { 'country_name' => 'SOLOMON ISLANDS                             ', 'iso_code' => 'SB', 'id' =>   90 },
  { 'country_name' => 'SOMALIA                                     ', 'iso_code' => 'SO', 'id' =>  706 },
  { 'country_name' => 'SOUTH AFRICA                                ', 'iso_code' => 'ZA', 'id' =>  710 },
  { 'country_name' => 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'iso_code' => 'GS', 'id' =>  239 },
  { 'country_name' => 'SPAIN                                       ', 'iso_code' => 'ES', 'id' =>  724 },
  { 'country_name' => 'SRI LANKA                                   ', 'iso_code' => 'LK', 'id' =>  144 },
  { 'country_name' => 'SUDAN                                       ', 'iso_code' => 'SD', 'id' =>  736 },
  { 'country_name' => 'SURINAME                                    ', 'iso_code' => 'SR', 'id' =>  740 },
  { 'country_name' => 'SVALBARD AND JAN MAYEN ISLANDS              ', 'iso_code' => 'SJ', 'id' =>  744 },
  { 'country_name' => 'SWAZILAND                                   ', 'iso_code' => 'SZ', 'id' =>  748 },
  { 'country_name' => 'SWEDEN                                      ', 'iso_code' => 'SE', 'id' =>  752 },
  { 'country_name' => 'SWITZERLAND                                 ', 'iso_code' => 'CH', 'id' =>  756 },
  { 'country_name' => 'SYRIAN ARAB REPUBLIC                        ', 'iso_code' => 'SY', 'id' =>  760 },
  { 'country_name' => 'TAIWAN                                      ', 'iso_code' => 'TW', 'id' =>  158 },
  { 'country_name' => 'TAJIKISTAN                                  ', 'iso_code' => 'TJ', 'id' =>  762 },  
  { 'country_name' => 'TANZANIA, UNITED REPUBLIC OF                ', 'iso_code' => 'TZ', 'id' =>  834 },
  { 'country_name' => 'THAILAND                                    ', 'iso_code' => 'TH', 'id' =>  764 },
  { 'country_name' => 'TIMOR-LESTE                                 ', 'iso_code' => 'TL', 'id' =>  626 },
  { 'country_name' => 'TOGO                                        ', 'iso_code' => 'TG', 'id' =>  768 },
  { 'country_name' => 'TOKELAU                                     ', 'iso_code' => 'TK', 'id' =>  772 },
  { 'country_name' => 'TONGA                                       ', 'iso_code' => 'TO', 'id' =>  776 },
  { 'country_name' => 'TRINIDAD AND TOBAGO                         ', 'iso_code' => 'TT', 'id' =>  780 },
  { 'country_name' => 'TUNISIA                                     ', 'iso_code' => 'TN', 'id' =>  788 },
  { 'country_name' => 'TURKEY                                      ', 'iso_code' => 'TR', 'id' =>  792 },
  { 'country_name' => 'TURKMENISTAN                                ', 'iso_code' => 'TM', 'id' =>  795 },  
  { 'country_name' => 'TURKS AND CAICOS ISLANDS                    ', 'iso_code' => 'TC', 'id' =>  796 },
  { 'country_name' => 'TUVALU                                      ', 'iso_code' => 'TV', 'id' =>  798 },
  { 'country_name' => 'UGANDA                                      ', 'iso_code' => 'UG', 'id' =>  800 },
  { 'country_name' => 'UKRAINE                                     ', 'iso_code' => 'UA', 'id' =>  804 },
  { 'country_name' => 'UNITED ARAB EMIRATES                        ', 'iso_code' => 'AE', 'id' =>  784 },
  { 'country_name' => 'UNITED KINGDOM                              ', 'iso_code' => 'GB', 'id' =>  826 },
  { 'country_name' => 'UNITED STATES                               ', 'iso_code' => 'US', 'id' =>  840 },
  { 'country_name' => 'UNITED STATES MINOR OUTLYING ISLANDS        ', 'iso_code' => 'UM', 'id' =>  581 },
  { 'country_name' => 'URUGUAY                                     ', 'iso_code' => 'UY', 'id' =>  858 },
  { 'country_name' => 'UZBEKISTAN                                  ', 'iso_code' => 'UZ', 'id' =>  860 },  
  { 'country_name' => 'VANUATU                                     ', 'iso_code' => 'VU', 'id' =>  548 },
  { 'country_name' => 'VATICAN CITY STATE (HOLY SEE)               ', 'iso_code' => 'VA', 'id' =>  336 },
  { 'country_name' => 'VENEZUELA                                   ', 'iso_code' => 'VE', 'id' =>  862 },
  { 'country_name' => 'VIET NAM                                    ', 'iso_code' => 'VN', 'id' =>  704 },
  { 'country_name' => 'VIRGIN ISLANDS (BRITISH)                    ', 'iso_code' => 'VG', 'id' =>   92 },
  { 'country_name' => 'VIRGIN ISLANDS (U.S.)                       ', 'iso_code' => 'VI', 'id' =>  850 },
  { 'country_name' => 'WALLIS AND FUTUNA ISLANDS                   ', 'iso_code' => 'WF', 'id' =>  876 },
  { 'country_name' => 'WESTERN SAHARA                              ', 'iso_code' => 'EH', 'id' =>  732 },
  { 'country_name' => 'YEMEN                                       ', 'iso_code' => 'YE', 'id' =>  887 },
  { 'country_name' => 'ZAMBIA                                      ', 'iso_code' => 'ZM', 'id' =>  894 },
  { 'country_name' => 'ZIMBABWE                                    ', 'iso_code' => 'ZW', 'id' =>  716 }
]

# Create city information
cities = [
  {'city_name' => 'Dublin'    , 'country_id' => 327, 'id' => 3271},
  {'city_name' => 'New York'  , 'country_id' => 840, 'id' => 8401},
  {'city_name' => 'São Paulo' , 'country_id' => 86,  'id' => 861},
  {'city_name' => 'London'    , 'country_id' => 826, 'id' => 8261},
  {'city_name' => 'Berlin'    , 'country_id' => 268, 'id' => 2681},
  {'city_name' => 'Madrid'    , 'country_id' => 724, 'id' => 7241},
  {'city_name' => 'Toquio'    , 'country_id' => 392, 'id' => 3981},
  {'city_name' => 'Amsterdam' , 'country_id' => 528, 'id' => 5281}
]

# Create trips
trips = [
  {'start_date' => Date.new(2014, 01, 01), 'end_date' => Date.new(2014, 01, 05), 'city_id' => 8401}, #  1 Barack Obama to New York
  {'start_date' => Date.new(2014, 02, 01), 'end_date' => Date.new(2014, 02, 10), 'city_id' => 3271}  #  2 Herry and Peter to Dublin
]

# Create users
users = [
  {'user_name' => 'Barack Obama', 'email' => 'barack@whitehouse.com'      , 'password' => '123456', 'password_confirmation' => '123456'}, # 1
  {'user_name' => 'Herry Potter', 'email' => 'herry@hogwarts.edu'         , 'password' => '654321', 'password_confirmation' => '654321'}, # 2
  {'user_name' => 'Peter Parker', 'email' => 'spiderman@justiceleague.com', 'password' => '654321', 'password_confirmation' => '654321'}  # 3
]

# Create User and Trip relashionship
user_trip_rels = [
  {'user_id' => 1, 'trip_id' => 1}, # 1
  {'user_id' => 2, 'trip_id' => 2}, # 2
  {'user_id' => 3, 'trip_id' => 2}  # 3
]

# Create expense categories
expense_categories = [
  {'description' => 'Hotels, hostels and other accommodation costs', 'category_name' => 'Accommodation'}, # 1
  {'description' => 'Local transport in the city',                   'category_name' => 'Transport'},     # 2
  {'description' => 'Food costs',                                    'category_name' => 'Food'},          # 3
  {'description' => 'Entertainment costs',                           'category_name' => 'Entertainment'}  # 4
]

# Create currency information
currencies =[
  {'iso_code' => 'USD', 'currency_name' => 'US Dollar'},        # 1
  {'iso_code' => 'EUR', 'currency_name' => 'Euro'},             # 2
  {'iso_code' => 'GBP', 'currency_name' => 'Britsh Pound'},     # 3
  {'iso_code' => 'AUS', 'currency_name' => 'Australian Dolla'}, # 4
  {'iso_code' => 'CAD', 'currency_name' => 'Canadian Dollar'}   # 5
]

# Create expenses
expenses = [
  # Barack Obama expenses on trip (1) to New York
  {'date_time' => DateTime.new(2014, 01, 01, 10, 0, 0, '-5'), 'amount' => 6.59,   'currency_id' => 1, 'expense_category_id' => 3, 'trip_id' => 1, 'user_id' => 1}, # 1
  {'date_time' => DateTime.new(2014, 01, 01, 15, 0, 0, '-5'), 'amount' => 70.00,  'currency_id' => 1, 'expense_category_id' => 4, 'trip_id' => 1, 'user_id' => 1}, # 2
  {'date_time' => DateTime.new(2014, 01, 02, 12, 0, 0, '-5'), 'amount' => 100.00, 'currency_id' => 1, 'expense_category_id' => 1, 'trip_id' => 1, 'user_id' => 1}, # 3
  {'date_time' => DateTime.new(2014, 01, 03, 17, 0, 0, '-5'), 'amount' => 16.59,  'currency_id' => 1, 'expense_category_id' => 2, 'trip_id' => 1, 'user_id' => 1}, # 4

  # Petter Parker and Herry Poter expenses on trip (2) to Dublin
  {'date_time' => DateTime.new(2014, 02, 01, 10, 0, 0, '0'), 'amount' => 16.59,  'currency_id' => 2, 'expense_category_id' => 3, 'trip_id' => 2, 'user_id' => 2},   # 5
  {'date_time' => DateTime.new(2014, 02, 01, 15, 0, 0, '0'), 'amount' => 170.00, 'currency_id' => 2, 'expense_category_id' => 4, 'trip_id' => 2, 'user_id' => 2},   # 6
  {'date_time' => DateTime.new(2014, 02, 02, 12, 0, 0, '0'), 'amount' => 150.00, 'currency_id' => 2, 'expense_category_id' => 1, 'trip_id' => 2, 'user_id' => 2},   # 7
  {'date_time' => DateTime.new(2014, 02, 02, 17, 0, 0, '0'), 'amount' => 160.00, 'currency_id' => 2, 'expense_category_id' => 2, 'trip_id' => 2, 'user_id' => 2},   # 8
  {'date_time' => DateTime.new(2014, 02, 03, 11, 0, 0, '0'), 'amount' => 160.59,  'currency_id' => 2, 'expense_category_id' => 1, 'trip_id' => 2, 'user_id' => 3},  # 9
  {'date_time' => DateTime.new(2014, 02, 04, 12, 0, 0, '0'), 'amount' => 15.00,   'currency_id' => 2, 'expense_category_id' => 2, 'trip_id' => 2, 'user_id' => 3},  # 10
  {'date_time' => DateTime.new(2014, 02, 05, 13, 0, 0, '0'), 'amount' => 10.00,   'currency_id' => 2, 'expense_category_id' => 3, 'trip_id' => 2, 'user_id' => 3},  # 11
  {'date_time' => DateTime.new(2014, 02, 05, 23, 0, 0, '0'), 'amount' => 150.00,  'currency_id' => 2, 'expense_category_id' => 4, 'trip_id' => 2, 'user_id' => 3}   # 12
]

# Insert data in database
Country.create(countries, :without_protection => true)
City.create(cities, :without_protection => true)
Trip.create(trips, :without_protection => true)
User.create(users,     :without_protection => true)
UserTripRel.create(user_trip_rels, :without_protection => true)
ExpenseCategory.create(expense_categories, :without_protection => true)
Currency.create(currencies, :without_protection => true)
Expense.create(expenses, :without_protection => true)