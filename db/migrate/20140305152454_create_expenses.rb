class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.references :trip
      t.references :expense_category
      t.references :currency
      t.references :user
      t.float :amount
      t.datetime :date_time

      t.timestamps
    end
    add_index :expenses, :trip_id
    add_index :expenses, :expense_category_id
    add_index :expenses, :currency_id
    add_index :expenses, :user_id
  end
end
