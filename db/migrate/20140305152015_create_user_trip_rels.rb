class CreateUserTripRels < ActiveRecord::Migration
  def change
    create_table :user_trip_rels do |t|
      t.references :user
      t.references :trip

      t.timestamps
    end
    add_index :user_trip_rels, :user_id
    add_index :user_trip_rels, :trip_id
  end
end
