class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.date :start_date
      t.date :end_date
      t.references :city

      t.timestamps
    end
    add_index :trips, :city_id
  end
end
