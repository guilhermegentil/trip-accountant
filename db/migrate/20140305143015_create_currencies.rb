class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :currency_name
      t.string :iso_code

      t.timestamps
    end
  end
end
