class CreateApiUsers < ActiveRecord::Migration
  def change
    create_table :api_users do |t|
      t.string :api_user_name
      t.string :email
      t.string :password_digest
      t.string :api_key

      t.timestamps
    end
      add_index :api_users, :api_key
  end
end
