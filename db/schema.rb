# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140313125435) do

  create_table "api_users", :force => true do |t|
    t.string   "api_user_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "api_key"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "cities", :force => true do |t|
    t.string   "city_name"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "cities", ["country_id"], :name => "index_cities_on_country_id"

  create_table "countries", :force => true do |t|
    t.string   "country_name"
    t.string   "iso_code"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "currencies", :force => true do |t|
    t.string   "currency_name"
    t.string   "iso_code"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "expense_categories", :force => true do |t|
    t.string   "category_name"
    t.text     "description"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "expenses", :force => true do |t|
    t.integer  "trip_id"
    t.integer  "expense_category_id"
    t.integer  "currency_id"
    t.integer  "user_id"
    t.float    "amount"
    t.datetime "date_time"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "expenses", ["currency_id"], :name => "index_expenses_on_currency_id"
  add_index "expenses", ["expense_category_id"], :name => "index_expenses_on_expense_category_id"
  add_index "expenses", ["trip_id"], :name => "index_expenses_on_trip_id"
  add_index "expenses", ["user_id"], :name => "index_expenses_on_user_id"

  create_table "trips", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "city_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "trips", ["city_id"], :name => "index_trips_on_city_id"

  create_table "user_trip_rels", :force => true do |t|
    t.integer  "user_id"
    t.integer  "trip_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_trip_rels", ["trip_id"], :name => "index_user_trip_rels_on_trip_id"
  add_index "user_trip_rels", ["user_id"], :name => "index_user_trip_rels_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "user_name"
    t.string   "password_digest"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

end
