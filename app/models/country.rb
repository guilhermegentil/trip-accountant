class Country < ActiveRecord::Base
  has_many :cities
  attr_accessible :iso_code, :country_name

  validates :country_name, presence: true
  validates :iso_code, presence: true
end
