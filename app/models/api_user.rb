class ApiUser < ActiveRecord::Base
  attr_accessible :api_key, :api_user_name, :email, :password, :password_confirmation

  # Allowed by gem bcrypt-ruby to encypt passwords.
  has_secure_password

  # Do some information treatment before save it on database.
  before_save do |user|
      user.api_key = SecureRandom.urlsafe_base64 # Generates a unique key for each user.
  end

  # REGEX to validate emails strings with the format [STRING]@[STRING].[STRING]
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  # Field validations
  validates :api_user_name, presence: true, length: { minimum:1, maximum:50 }
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum:6 }
  validates :password_confirmation, presence:true
end
