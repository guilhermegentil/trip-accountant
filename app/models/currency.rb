class Currency < ActiveRecord::Base
  has_many :expenses
  attr_accessible :iso_code, :currency_name

  validates :currency_name, presence: true
  validates :iso_code, presence: true
end
