class Expense < ActiveRecord::Base
  belongs_to :trip
  belongs_to :expense_category
  belongs_to :currency
  belongs_to :user
  attr_accessible :date_time, :amount

  validates :user_id, presence: true
  validates :trip_id, presence: true
  validates :expense_category_id, presence: true
  validates :currency_id, presence: true
  validates :amount, presence: true

end