class Trip < ActiveRecord::Base
  has_many :expenses
  has_many :user_trip_rels
  has_many :users, through: :user_trip_rels

  belongs_to :city
  attr_accessible :end_date, :start_date

  validates :city_id, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
end
