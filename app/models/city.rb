class City < ActiveRecord::Base
  belongs_to :country
  has_many :trips
  attr_accessible :city_name

  validates :city_name, presence: true
  validates :country_id, presence: true
end
