class User < ActiveRecord::Base
  
  has_many :expenses
  has_many :user_trip_rels
  has_many :trips, through: :user_trip_rels

  attr_accessible :email, :user_name, :password, :password_confirmation

  # Allowed by gem bcrypt-ruby to encypt passwords.
  has_secure_password

  # Do some information treatment before save it on database.
  before_save do |user|
      user.email = email.downcase # To avoid conflicts on database store all emails as lower case.
  end

  # REGEX to validate emails strings with the format [STRING]@[STRING].[STRING]
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  # Field validations
  validates :user_name, presence: true, length: { minimum:1, maximum:50 }
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum:6 }
  validates :password_confirmation, presence:true

end
