class ExpenseCategory < ActiveRecord::Base
  has_many :expenses
  attr_accessible :description, :category_name

  validates :category_name, presence: true
  
end
