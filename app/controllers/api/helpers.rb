require 'grape'
#require '3scale/client'

module API
  module Helpers

    def authenticate!
      if params[:api_key]
        api_user_token = ApiUser.find_by_sql("SELECT api_key FROM api_users WHERE api_key = '" + params[:api_key] + "' LIMIT 1;")
        error!('403 Unauthorized: invalid api_key', 403) if api_user_token.empty?
      else
        error!('404 Please provide an valid api_key and attach it as an query parameter. ie: http://url.something?api_key=-VALID-KEY-', 404)
      end
    end

    def check_resource_exists(resource, id)
      unless resource
        error_resource_not_found(id)
      end
    end

    def helper_log(level, msg)
      msg = "[#{level}][#{Time.now} - #{msg}]" 
      case level
      when :debug
        Rails.logger.debug(msg)
      when :info
        Rails.logger.info(msg)
      when :warn
        Rails.logger.warn(msg)
      when :error
        Rails.logger.error(msg)
      when :fatal
        Rails.logger.fatal(msg)
      end
    end

    def error_resource_not_found(id)
      msg = "No resource with id: #{id}"
      helper_log(:error, msg)
      error!(msg , 404)
    end

    def error_resource_not_created()
      msg = "Resource not created"
      helper_log(:error, msg)
      error!(msg , 404)
    end

  end
end