require 'grape'

module API
  module V1
    class Countries < Grape::API

      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :countries do 

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        # GET /api/countries
        desc 'Return all countries'
        get do
          helper_log(:info, 'Processing GET /api/countries')
          Country.all
        end

        # validat the ID on the request
        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        namespace ':id' do
          # GET /api/v1/countries/:id
          desc 'Return a country by a given id.'
          get do
            helper_log(:info, "Processing DELETE /api/v1/countries/#{params[:id]}")
            @country = Country.find_by_id(params[:id])
            check_resource_exists(@country, params[:id])
            @country
          end

        end # namespace :id
      end # namespace :countries
    end # class Countries
  end # module V1
end # module API