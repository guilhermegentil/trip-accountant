require 'grape'

module API
  module V1
    class Users < Grape::API
      
      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :users do

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        # GET /api/v1/users
        desc 'Return all users.'
        get do
          helper_log(:info, "Processing GET /api/v1/users")
          User.all
        end

        # Validation of POST parameters
        params do 
          requires :user_name,             type: String, desc: 'The name of the user.'
          requires :email,                 type: String, desc: 'The email of the user.'
          requires :password,              type: String, desc: 'The password of the user.'
          requires :password_confirmation, type: String, desc: 'The password confirmation of the user.'
        end

        # POST /api/v1/users
        desc 'Create a new user.'
        post do
          helper_log(:info, "Processing POST /api/v1/users")
          @user = User.new
          @user.user_name = params[:user_name] if params[:user_name]
          @user.email = params[:email] if params[:email]
          @user.password = params[:password] if params[:password]
          @user.password_confirmation = params[:password_confirmation] if params[:password_confirmation]

          if @user.save
            helper_log(:info, "User #{@user.id} created!")
            status 201
          else
            error_resource_not_created()
          end
          @user
        end

        # validade the ID on the request
        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        # retrieve information about a specific user
        namespace ':id' do
          # GET /api/v1/users/:id
          desc 'Return an user by a given id.'
          get do
            helper_log(:info, "Processing GET /api/v1/users/#{params[:id]}")
            @user = User.find_by_id(params[:id])
            check_resource_exists(@user, params[:id])
            @user
          end

          desc 'Delete an user by a given id.'
          delete do
            helper_log(:info, "Processing DELETE /api/v1/users/#{params[:id]}")
            helper_log(:info, "Deleting user with id: #{params[:id]}")
            @user = User.find_by_id(params[:id])
            check_resource_exists(@user, params[:id])
            @user.destroy
          end

          # retrieve information about trips
          namespace :trips do
            # GET /api/v1/users/:id/trips
            desc 'Return all the trips made by an specific user.'
            get do
              helper_log(:info, "Processing GET /api/v1/users/#{params[:id]}/trips")
              @user = User.find_by_id(params[:id])
              check_resource_exists(@user, params[:id])
              @user.trips
            end
          end # namespace : trips
        end # namespace :id
      end # namespace :users
    end # class Users
  end # module V1
end # module API