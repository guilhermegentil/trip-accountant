require 'grape'

module API
  module V1
    class ExpenseCategories < Grape::API

      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :expense_categories do 

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        desc 'Return all categories'
        get do
          helper_log(:info, 'Processing GET /api/v1/expense_categories')
          ExpenseCategory.all
        end

        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        namespace ':id' do

          desc 'Return details about a specific category.'
          get do
            helper_log(:info, "Processing GET /api/v1/expense_categories/#{params[:id]}")
            @category = ExpenseCategory.find_by_id(params[:id])
            check_resource_exists(@category, params[:id])
            @category
          end

        end # namespace :id
      end # namespace :expense_categories
    end # class ExpenseCategories
  end # module V1
end # module API