require 'grape'

module API
  module V1
    class Currencies < Grape::API

      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :currencies do 

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        desc 'Return all currencies'
        get do
          helper_log(:info, 'Processing GET /api/v1/currencies')
          Currency.all
        end

        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        namespace ':id' do

          desc 'Return details about a specific currency.'
          get do
            helper_log(:info, "Processing GET /api/v1/currencies/#{params[:id]}")
            @currency = Currency.find_by_id(params[:id])
            check_resource_exists(@currency, params[:id])
            @currency
          end

        end # namespace :id
      end # namespace :currencies
    end # class Currencies
  end # module V1
end # module API