require 'grape'

module API
  module V1
    class Base < Grape::API
      mount API::V1::Users
      mount API::V1::Trips
      mount API::V1::Cities
      mount API::V1::Countries
      mount API::V1::Currencies
      mount API::V1::ExpenseCategories
      mount API::V1::Expenses      

      # Adds the swagger documentation to your
      add_swagger_documentation(
        base_path: '/api',
        api_version: 'v1',
        hide_documentation_path: true
      )
    end
  end
end