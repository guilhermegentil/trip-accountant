require 'grape'

module API
  module V1
    class Trips < Grape::API
      
      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :trips do

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        # GET /api/v1/trips
        desc 'Return all users.'
        get do
          helper_log(:info, "Processing GET /api/v1/trips")
          Trip.all
        end

        # Validation of POST parameters
        params do 
          requires :start_date, type: Date   , desc: 'The first day of the trip.'
          requires :end_date,   type: Date   , desc: 'The last day of the trip.'
          requires :city_id,    type: Integer, desc: 'The city of the trip.'
          requires :user_ids,   type: Array  , desc: 'An Array with the ids of the users.'
        end

        # POST /api/v1/trips
        desc 'Create a new trip.'
        post do
          helper_log(:info, 'Processing POST /api/v1/trips')

          @trip = Trip.new
          @trip.start_date = params[:start_date] if params[:start_date]
          @trip.end_date   = params[:end_date]   if params[:end_date]
          @trip.city_id    = params[:city_id]    if params[:city_id]
          @trip.save

          # Get the ids of the lit of users
          user_ids = params[:user_ids]

          unless user_ids.size > 0
            helper_log(:warn, "Must have at least one user per trip, user_ids can't be empty!")
            @trip.destroy
            error_resource_not_created()
          end

          user_ids.each { |id|
            helper_log(:debug, "Retrieving user with id #{id} from database")
            existing_user = User.find_by_id(id)

            unless existing_user
              helper_log(:warn, "User with id #{id} doesn't exist")

              # Can't create a new trip unless all users are valid
              @trip.destroy
              error_resource_not_found(id)
            end

            @trip.users.append(existing_user)
            helper_log(:debug, "User #{existing_user} added to the trip #{@trip}")
          }

          if @trip.save
            helper_log(:info, "New trip created: #{@trip}")
            status 201
          else
            error_resource_not_created()
          end
          @trip
        end # post

        # validade the ID on the request
        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        # retrieve information about a specific trip
        namespace ':id' do
          # GET /api/v1/trips/:id
          desc 'Return an trip by a given id.'
          get do
            helper_log(:info, "Processing GET /api/v1/trips/#{params[:id]}")
            @trip = Trip.find_by_id(params[:id])
            check_resource_exists(@trip, params[:id])
            @trip
          end

          # GET /api/v1/trips/users
          desc 'Return all users from a trip.'
          get '/users' do
            helper_log(:info, "Processing GET /api/v1/trips/#{params[:id]}/users")
            @trip = Trip.find_by_id(params[:id])
            check_resource_exists(@trip, params[:id])
            @trip.users
          end

          # GET /api/v1/trips/expenses
          desc 'Return all the expenses made in a trip.'
          get '/expenses' do
            helper_log(:info, "Processing GET /api/v1/trips/#{params[:id]}/expenses")
            @trip = Trip.find_by_id(params[:id])
            check_resource_exists(@trip, params[:id])
            @trip.expenses
          end

          namespace :city do
            # GET /api/v1/trips/city
            desc 'Return the city of a trip.'
            get do
              helper_log(:info, "Processing GET /api/v1/trips/city")
              @trip = Trip.find_by_id(params[:id])
              check_resource_exists(@trip, params[:id])
              @trip.city
            end

            # GET /api/v1/trips/city/country
            desc 'Return the country of a city in a trip.'
            get '/country' do
              helper_log(:info, "Processing GET /api/v1/trips/city/country")
              @trip = Trip.find_by_id(params[:id])
              check_resource_exists(@trip, params[:id])
              @trip.city.country
            end
          end # namespace :city
        end # namespace :id
      end # namespace :trips
    end # class Trip
  end # module V1
end # module API