require 'grape'

module API
  module V1
    class Expenses < Grape::API
     
      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      # Validation of POST parameters
      params do
        requires :user_id,             type: Integer, desc: 'The user id of the user who made the expense.'
        requires :trip_id,             type: Integer, desc: 'The id of the trip where the expense were made.'
        requires :expense_category_id, type: Integer, desc: 'The category of the expense.'
        requires :currency_id,         type: Integer, desc: 'The type of the expense.'
        requires :amount,              type: Float,   desc: 'The actual value of th exoense.'
        optional :date_time,           type: Time,    desc: 'The specific time when the expense were made.', default: Time.now
      end

      namespace :expenses do

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        # POST /api/expenses
        desc 'Create a new expense'
        post do
          helper_log(:info, 'Processing POST /api/expenses')

          @expense = Expense.new
          @expense.user_id = params[:user_id] if params[:user_id]
          @expense.trip_id = params[:trip_id] if params[:trip_id]
          @expense.expense_category_id = params[:expense_category_id] if params[:expense_category_id]
          @expense.currency_id = params[:currency_id] if params[:currency_id]
          @expense.amount = params[:amount] if params[:amount]
          @expense.date_time = params[:date_time] if params[:date_time]

          if @expense.save
            helper_log(:info, "New expense created: #{@expense}")
            status 201
          end
          @expense
        end 

      end # namespace :expense
    end # class Expenses
  end # module V1
end # module API
