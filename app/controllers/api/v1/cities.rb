require 'grape'

module API
  module V1
    class Cities < Grape::API

      def self.get_daily_average(city, expense_category=nil, start_date=nil, end_date=nil)

        unless city.trips.size > 0
          error!("No trips found to this city: #{city.id}", 404)
        end

        total_cost = 0.0

        # Get all trips to this city
        trips = city.trips

        # Get the oldest and newest date registered to calculate the number of days
        # in order to calculate the average cost per day in that city
        newest_date = trips.order('start_date ASC')[0].end_date
        oldest_date = trips.order('end_date DESC')[0].start_date

        if start_date && end_date
          newest_date = end_date
          oldest_date = start_date
        end
        
        # Total number of days spend in that city
        num_of_days = newest_date - oldest_date

        trips.each{ |t|          
          t.expenses.each{ |e|
            if expense_category
              total_cost += e.amount if e.expense_category == expense_category
            else
              total_cost += e.amount
            end
            #e.expense_category == expense_category && total_cost += e.amount || total_cost += 0
          }
        }

        # Return the average
        average = total_cost / num_of_days
      end

      # helper methods
      helpers API::Helpers

      # common Grape settings
      version 'v1', usign: :path
      format :json

      namespace :cities do

        # execute this block before every call to an API's endpoint
        before do
          # authenticate the API key provided in the URL (param[:api_key])
          authenticate!
        end

        # GET /api/v1/cities
        desc 'Return all cities.'
        get do
          helper_log(:info, "Processing GET /api/v1/cities")
          City.all
        end

        # validade the ID on the request
        params do
          requires :id, type: Integer, desc: 'The resource id.'
        end

        namespace ':id' do

          # GET /api/v1/:id
          desc 'Return a city by a given id.'
          get do
            helper_log(:info, "Processing GET /api/v1/#{params[:id]}")
            @city = City.find_by_id(params[:id])
            check_resource_exists(@city, params[:id])
            @city
          end
        
          namespace :costs do

            # GET /api/v1/:id/costs
            desc 'Return the average daily cost considering all categories.'
            get do
              helper_log(:info, "Processing GET /api/v1/#{params[:id]}/costs")

              city = City.find_by_id(params[:id])
              check_resource_exists(city, params[:id])

              Cities.get_daily_average(city)

            end # get

            params do
              requires :category,   type: Integer, desc: 'The id of a category.'
              optional :start_date, type: Date,    desc: 'First day of the period.'
              optional :end_date,   type: Date,    desc: 'Last day of the period.'
            end

            namespace ':category' do
              
              # GET /api/v1/:id/costs/:category
              desc 'Return the average daily cost considering one category.'
              get do
                helper_log(:info, "Processing GET /api/v1/#{params[:id]}/costs/#{params[:category]}")

                city = City.find_by_id(params[:id])
                check_resource_exists(city, params[:id])

                expense_category = ExpenseCategory.find_by_id(params[:category])
                check_resource_exists(expense_category, params[:category])

                if params[:start_date] && params[:end_date]
                  Cities.get_daily_average(city, expense_category, params[:start_date], params[:end_date])
                else
                  Cities.get_daily_average(city, expense_category)
                end
              end # get

            end # namespace :category
          end # namespace :costs
        end # namespace :cities
      end # namespace :id
    end # class Cities
  end # module V1
end # module API